package com.example.yatri.linechart;

import android.util.Log;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by philipp on 02/06/16.
 */
public class DayAxisValueFormatter extends ValueFormatter {

    private final String[] mMonths = new String[]{
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };
    ArrayList<String> dailyLable = new ArrayList<String>();
    private final BarLineChartBase<?> chart;
    String finall;

    public DayAxisValueFormatter(BarLineChartBase<?> chart) {
        this.chart = chart;
    }

//    private String dayFormater() {
//        DateFormat df = new SimpleDateFormat("hh.mm aa");
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.HOUR_OF_DAY, 0);
//        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.SECOND, 0);
//        int startDate = cal.get(Calendar.DATE);
//        while (cal.get(Calendar.DATE) == startDate) {
//            Log.e("TAG", df.format(cal.getTime()));
//            System.out.println(df.format(cal.getTime()));
//            cal.add(Calendar.MINUTE, 240);
//        }
//    }

    @Override
    public String getFormattedValue(float value) {
        int days = (int) value;

        int year = determineYear(days);

        int month = determineMonth(days);
        String monthName = mMonths[month % mMonths.length];
        String yearName = String.valueOf(year);

        if (chart.getVisibleXRange() > 30 * 6) {

            return monthName + " " + yearName;
        } else {

            dayFormater();

            return dailyLable.get(days - 1);
        }
    }

    private int getDaysForMonth(int month, int year) {

        // month is 0-based

        if (month == 1) {
            boolean is29Feb = false;

            if (year < 1582)
                is29Feb = (year < 1 ? year + 1 : year) % 4 == 0;
            else if (year > 1582)
                is29Feb = year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);

            return is29Feb ? 29 : 28;
        }

        if (month == 3 || month == 5 || month == 8 || month == 10)
            return 30;
        else
            return 31;
    }

    private void dayFormater() {

        DateFormat df = new SimpleDateFormat("hh.mm aa");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        int startDate = cal.get(Calendar.DATE);
        while (cal.get(Calendar.DATE) == startDate) {
            Log.e("TAG", df.format(cal.getTime()));
            System.out.println(df.format(cal.getTime()));
            String s = df.format(cal.getTime());
            s = s.replace("AM", "");
            s = s.replace("PM", "");
            cal.add(Calendar.MINUTE, 240);
            String a = df.format(cal.getTime());
            Log.e("TAG", "a vlaue:" + a);
            Log.e("TAG", "Final: " + s + "- " + a.toLowerCase());
            finall = s + "- " + a.toLowerCase();
            dailyLable.add(finall);
            Log.e("TAG", "@@:" + dailyLable.get(0));
        }
    }

    private void weelFormater() {

//        for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
    }
//    }

    private int determineMonth(int dayOfYear) {

        int month = -1;
        int days = 0;

        while (days < dayOfYear) {
            month = month + 1;

            if (month >= 12)
                month = 0;

            int year = determineYear(days);
            days += getDaysForMonth(month, year);
        }

        return Math.max(month, 0);
    }

    private int determineDayOfMonth(int days, int month) {

        int count = 0;
        int daysForMonths = 0;

        while (count < month) {

            int year = determineYear(daysForMonths);
            daysForMonths += getDaysForMonth(count % 12, year);
            count++;
        }

        return days - daysForMonths;
    }


    private int determineYear(int days) {

        if (days <= 366)
            return 2016;
        else if (days <= 730)
            return 2017;
        else if (days <= 1094)
            return 2018;
        else if (days <= 1458)
            return 2019;
        else
            return 2020;

    }
}
